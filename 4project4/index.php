<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$abils = ['god' => 'Бессмертие','noclip' => 'Проход сквозь стены','fly' => 'Левитация'];
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['mail'] = !empty($_COOKIE['mail_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['pol'] = !empty($_COOKIE['pol_error']);
  $errors['konec'] = !empty($_COOKIE['konec_error']);
  $errors['abils'] = !empty($_COOKIE['abils_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);
  
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    if($_COOKIE['fio_error'] == '1') {
        $messages[] = '<div class="error">Заполните имя.</div>';
    }
    else {
        $messages[] = '<div class="error">Укажите корректное имя (Кириллицей).</div>';
    }
  }
  
  if ($errors['mail']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('mail_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['mail_error'] == '1') {
      $messages[] = '<div class="error">Заполните e-mail.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректный e-mail.</div>';
      }
  }
  if ($errors['year']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('year_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['year_error'] == '1') {
          $messages[] = '<div class="error">Укажите год.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректный год.</div>';
      }
  }
  
  if ($errors['pol']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('pol_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Укажите пол.</div>';
  }
  
  if ($errors['konec']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('konec_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Укажите количество конечностей.</div>';
  }
  
  if ($errors['abils']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('$abils_error', '', 100000);
      // Выводим сообщение.
      if($_COOKIE['abils_error'] == '1') {
          $messages[] = '<div class="error">Выберите хотя бы 1 способность.</div>';
      }
      else {
          $messages[] = '<div class="error">Укажите корректную способность.</div>';
      }
  }
  
  if ($errors['bio']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('bio_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if ($errors['check']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('check_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Согласитесь с контрактом.</div>';
  }
  
  
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['mail'] = empty($_COOKIE['mail_value']) ? '' : $_COOKIE['mail_value'];
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['pol'] = empty($_COOKIE['pol_value']) ? '' : $_COOKIE['pol_value'];
  $values['konec'] = empty($_COOKIE['konec_value']) ? '' : $_COOKIE['konec_value'];
  if (!empty($_COOKIE['abils_value'])) {
  $abils_value = json_decode($_COOKIE['abils_value']);
  }
  $values['abils'] = [];
  if(isset($abils_value) && is_array($abils_value)) {
      foreach ($abils_value as $abil) {
          if(!empty($abils[$abil])) {
          $values['abils'][$abil] = $abil;
          }
      }
  }
  $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
  $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
  // TODO: аналогично все поля.

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
      if (!preg_match('/^[а-яА-Я ]+$/u',$_POST['fio'])) {
          setcookie('fio_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('fio_value', $_POST['fio'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['mail'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('mail_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      if (filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL) === false) {
          setcookie('mail_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('mail_value', $_POST['mail'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['year'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('year_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      if (!(is_numeric($_POST['year']) && intval($_POST['year']) >= 1900 && intval($_POST['year']) <= 2020)) {
          setcookie('year_error', '2', time() + 24 * 60 * 60);
          $errors = TRUE;
      }
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['pol'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('pol_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('pol_value', $_POST['pol'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['konec'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('konec_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('konec_value', $_POST['konec'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['abils'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('abils_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      $abils_error = FALSE;
      foreach ($_POST['abils'] as $a) {
          if (empty($abils[$a])) {
              setcookie('abils_error', '2', time() + 24 * 60 * 60);
              $errors = TRUE;
              $abils_error = TRUE;
          }
      }
      // Сохраняем ранее введенное в форму значение на год.
      if (!$abils_error) {
      setcookie('abils_value', json_encode($_POST['abils']), time() + 12 * 30 * 24 * 60 * 60);
      }
  }
  
  if (empty($_POST['bio'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('bio_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('bio_value', $_POST['bio'], time() + 12 * 30 * 24 * 60 * 60);
  }
  if (!isset($_POST['check'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('check_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на год.
      setcookie('check_value', $_POST['check'], time() + 12 * 30 * 24 * 60 * 60);
  }
  
// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('mail_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('pol_error', '', 100000);
    setcookie('konec_error', '', 100000);
    setcookie('abils_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('check_error', '', 100000);
    
    // TODO: тут необходимо удалить остальные Cookies.
  }

  // Сохранение в XML-документ.
  $user = 'u20336';
  $pass = '2383966';
  $db = new PDO('mysql:host=localhost;dbname=u20336', $user, $pass,
      array(PDO::ATTR_PERSISTENT => true));
  
  // Подготовленный запрос. Не именованные метки.
  try {
      $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, pol = ?, konec = ?, abilities = ?, biography = ?, checkbox = ?");
      $stmt ->execute([$_POST['fio'], $_POST['mail'], $_POST['year'] , $_POST['pol'], $_POST['konec'],  json_encode($_POST['abils']), $_POST['bio'], $_POST['check']] );
  }
  catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}

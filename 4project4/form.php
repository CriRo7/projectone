<html>
  <head>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
  </head>
  <body>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>

    <form action="" method="POST">
    Имя:<br>
      <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" /><br>
      E-mail:<br>
      <input name="mail" <?php if ($errors['mail']) {print 'class="error"';} ?> value="<?php print $values['mail']; ?>" /><br>
      Год:<br>
      <select name="year">
  <?php for ($i = 1900; $i <2020; $i++) { ?>
  <option value="<?php print $i;?>" <?= $i == $values['year'] ? 'selected' : '' ?>><?= $i;?></option>
  <?php } ?>
  <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>
  </select><br>
   Пол:<br>
        <input type="radio" checked  name="pol" id="radio1" value="M" <?= "M" == $values['pol'] ? 'checked="checked"' : '' ?>/>
          Мужской
        <input  type="radio" name="pol" id="radio2" value="Z" <?= "Z" == $values['pol'] ? 'checked="checked"' : '' ?> />
          Женский<br> 
           Количество конечностей:<br />
        <input type="radio" name="konec" checked id="radio3" value="1" <?= $values['konec'] == "1" ? 'checked="checked"' : '' ?>/>1 
        <input type="radio" name="konec"  id="radio4" value="2" <?= $values['konec'] == "2" ? 'checked="checked"' : '' ?>/>2
        <input type="radio" name="konec"  id="radio5" value="3" <?= $values['konec'] == "3" ? 'checked="checked"' : '' ?>/>3
        <input type="radio" name="konec"   id="radio6" value="4"  <?= $values['konec'] == "4" ? 'checked="checked"' : '' ?>/>4 <br>
         Cверхспособности:<br>
         <select name="abils[]" multiple <?php if ($errors['abils']) {print 'class="error"';} ?>>
        <?php  
        foreach ($abils as $key => $value) {
            $sel = empty($values['abils'][$key]) ? '' : ' selected="selected"';
            printf('<option value="%s"%s>%s</option>', $key, $sel, $value);
        }
        ?>
         </select><br>
          Биография:<br>
          <textarea name="bio" <?php if ($errors['bio']) {print 'class="error"';} ?> ><?php print $values['bio']; ?></textarea><br>
            Согласие:<br>
       <input type="checkbox" name="check" <?php if ($errors['check']) {print 'class="error"';} ?> <?= $values['check'] == "on" ? 'checked="checked"' : '' ?> />
          С контрактом ознакомлен<br>
      <input type="submit" value="ok" />
    </form>
  </body>
</html>
